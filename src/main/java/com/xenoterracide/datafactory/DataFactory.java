package com.xenoterracide.datafactory;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;
import java.util.Map;
import java.util.function.IntBinaryOperator;

import static com.xenoterracide.datafactory.DataFactoryUtil.valueFromMap;

public interface DataFactory {

    default RandomStringFactory numeric() {
        return ( min, max ) -> RandomStringUtils.randomNumeric( getRandom().applyAsInt( min, max ) );
    }

    IntBinaryOperator getRandom();

    default RandomStringFactory alphanumeric() {
        return ( min, max ) -> RandomStringUtils.randomAlphanumeric( getRandom().applyAsInt( min, max ) );
    }

    default RandomStringFactory graph() {
        return ( min, max ) -> RandomStringUtils.random( getRandom().applyAsInt( min, max ), 33, 126, false, false );
    }

    default RandomStringFactory print() {
        return ( min, max ) -> RandomStringUtils.random( getRandom().applyAsInt( min, max ), 32, 126, false, false );
    }

    default RandomStringFactory word() {
        return ( min, max ) -> valueFromMap( getWordSource(), ( size ) -> getRandom().applyAsInt( 0, size ), min, max )
                .orElse( alphabetic().getRange( min, max ) );


    }

    Map<Integer, List<String>> getWordSource();

    default RandomStringFactory alphabetic() {
        return ( min, max ) -> RandomStringUtils.randomAlphabetic( getRandom().applyAsInt( min, max ) );
    }
}
