package com.xenoterracide.datafactory;

import org.apache.commons.lang3.RandomUtils;

import java.util.List;
import java.util.Map;
import java.util.function.IntBinaryOperator;
import java.util.stream.Collectors;

import static com.xenoterracide.datafactory.DataFactoryUtil.dict;

public class DataFactoryImpl implements DataFactory {

    private final IntBinaryOperator random;
    private final Map<Integer, List<String>> wordSource;

    DataFactoryImpl( final IntBinaryOperator randomSource, final List<String> wordSource ) {
        this.random = randomSource;
        this.wordSource = wordSource.stream().collect( Collectors.groupingBy( String::length ) );
    }

    public static DataFactory defaultInstance() {
        return new DataFactoryImpl( RandomUtils::nextInt, dict() );
    }

    @Override
    public IntBinaryOperator getRandom() {
        return random;
    }

    @Override
    public Map<Integer, List<String>> getWordSource() {
        return wordSource;
    }

}
