package com.xenoterracide.datafactory;

import java.util.function.Function;
import java.util.function.Supplier;

public interface DataFactoryOld {

    default String getWord() {
        return getWordSupplier().get();
    }

    Supplier<String> getWordSupplier();

    default String getPage( final int maxInclusive ) {
        StringBuilder builder = new StringBuilder();
        int nextLength = builder.length();
        String next = "";
        String prev = "";
        while ( !( nextLength >= maxInclusive ) ) {
            if ( !prev.isEmpty() ) {
                builder.append( "\n\n" );
            }
            if ( !next.isEmpty() && !( nextLength >= maxInclusive ) ) {
                builder.append( next );
            }
            prev = next;
            next = this.getParagraph( 200 );
            nextLength = next.length() + builder.length() + 1;
        }
        return builder.toString();
    }

    default String getParagraph( final int maxInclusive ) {
        StringBuilder builder = new StringBuilder();
        int nextLength = builder.length();
        String next = "";
        String prev = "";
        while ( !( nextLength >= maxInclusive ) ) {
            if ( !prev.isEmpty() ) {
                builder.append( "  " );
            }
            if ( !next.isEmpty() && !( nextLength >= maxInclusive ) ) {
                builder.append( next );
            }
            prev = next;
            next = this.getSentence();
            nextLength = next.length() + builder.length() + 1;
        }
        return builder.toString();
    }

    default String getSentence() {
        // average sentence length is 14 words, average word is 5 characters
        return this.getSentence( 70 );
    }

    default String getSentence( final int maxInclusive ) {
        StringBuilder builder = new StringBuilder();
        int nextLength = builder.length();
        String next = "";
        String prev = "";
        while ( !( nextLength >= maxInclusive ) ) {
            if ( !prev.isEmpty() ) {
                builder.append( " " );
            }
            if ( !next.isEmpty() && !( nextLength >= maxInclusive ) ) {
                builder.append( next );
            }
            Integer randomInt = getRandomSupplier().apply( 10 );
            if ( randomInt % 3 == 0 ) {
                builder.append( this.getPunctuationSupplier().get() );
            }
            prev = next;
            next = getWordSupplier().get();
            nextLength = next.length() + builder.length() + 1;
        }
        return builder.toString();
    }

    Function<Integer, Integer> getRandomSupplier();

    Supplier<Character> getPunctuationSupplier();
}
