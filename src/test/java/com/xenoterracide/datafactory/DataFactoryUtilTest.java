package com.xenoterracide.datafactory;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;
import java.util.function.IntBinaryOperator;
import java.util.stream.IntStream;

import static com.xenoterracide.datafactory.DataFactoryUtil.dict;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

public class DataFactoryUtilTest {
    private static final int ITERATIONS = 100;
    private static final List<String> DICT = dict();
    private static final DataFactory RANDOM_DF = DataFactoryImpl.defaultInstance();
    private IntBinaryOperator random;
    private DataFactory nonRandomDf;

    @Before
    public void setup() {
        Iterator<Integer> iterator = IntStream.iterate( 0, ( i ) -> i + 1 ).iterator();
        random = ( min, max ) -> iterator.next();
        nonRandomDf = new DataFactoryImpl( random, DICT );
    }

    @Test
    public void testWord() throws Exception {
        RandomStringFactory factory = nonRandomDf.word();

        String word = factory.getRange( 1, 30 );

        assertThat( word, word.length(), lessThanOrEqualTo( 30 ) );
        assertThat( word, word, equalTo( "a" ) );

        word = factory.getRange( 1, 30 );
        assertThat( word, word.length(), lessThanOrEqualTo( 30 ) );
        assertThat( word, word, equalTo( "c" ) );

        for ( int i = 0; i < ITERATIONS; i++ ) {
            assertThat( factory.getRange( 1, 30 ), not( isEmptyOrNullString() ) );
            assertThat( RANDOM_DF.word().getRange( 1, 30 ), not( isEmptyOrNullString() ) );
        }

        word = factory.getRange( 1, 30 );
        assertThat( word, word.length(), lessThanOrEqualTo( 30 ) );
        assertThat( word, word, equalTo( "ani" ) );
    }
}
